﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SwitchOMatic.App_Start;
using SwitchOMatic.Respositories;
using SwitchOMatic.Services;

namespace SwitchOMatic.Tests.Repositories
{
    [TestClass]
    public class SwitchOMatic
    {

        //Normally we'd use a mocking framework but the repos are kind of like mocks

        private readonly ISwitchOMaticRepository _switchOMaticRepository;
        private readonly ISwitchOMaticService _switchOMaticService;
        
        public SwitchOMatic()
        {
             var container = DIEngine.GetNewContainer();
            _switchOMaticRepository = container.GetInstance<ISwitchOMaticRepository>();
            _switchOMaticService = container.GetInstance<ISwitchOMaticService>();
        }

        [TestMethod]
        public void TestGetResultsList()
        {
            var results = _switchOMaticRepository.GetResultsList();

            Assert.IsTrue(results.Count == 5); 
        }

        [TestMethod]
        public void TestGetResultsForID()
        {
            var results = _switchOMaticRepository.GetResultsByID(1003);

            Assert.IsTrue(results.results.Sum(x => x.TouchCount) == 1074);
            Assert.IsTrue(results.results.Where(x => x.Lit == true).Count() == 158);
        }

        [TestMethod]
        public void TestSaveResults()
        {
            var results  = _switchOMaticService.Run(new Models.SwitchOMaticSettings { LightCount = 100, PeopleCount = 100 });

            _switchOMaticRepository.SaveResults(results); 

        }

    }
}
