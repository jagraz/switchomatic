﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SwitchOMatic.App_Start;
using SwitchOMatic.Respositories;
using SwitchOMatic.Services;

namespace SwitchOMatic.Tests.Services
{
    [TestClass]
    public class SwitchOMatic
    {

        private readonly ISwitchOMaticService _switchOMaticService; 

        public SwitchOMatic()
        {
            var container = DIEngine.GetNewContainer();
            _switchOMaticService = container.GetInstance<ISwitchOMaticService>();
        }

        [TestMethod]
        public void SwitchOMaticTest()
        {
            var results = _switchOMaticService.Run(new Models.SwitchOMaticSettings { LightCount = 100, PeopleCount = 100 });

            Assert.IsTrue(results.results != null); 
            Assert.IsTrue(results.results.Where(x => x.Lit == true).Count() == 10);
            Assert.IsTrue(results.results.Sum(x => x.TouchCount) == 482); 
        }


    }
}
