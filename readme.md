#  Welcome to SwitchOMatic

SwitchOMatic is a tool to model people entering a room and switching lightbulbs. Why not crack open a cold one and have SwitchOMatic do the work?

## Structure

SwitchOMatic has a repository and service structure. Repos talk to (or mock) a data layer while services contain the business logic and consume the repos.

## Frameworks 
SwitchOMatic uses the Simpleinject DI framework for Dependency Injection. The DI Container is factored out into a DIEngine class for use in other services and projects; you can see it in action in the Testing project.

The UI is written in Bootstrap for mobile compatibility and ease of theming.

## Features
Features have been built or stubbed out:
 
* Ability to store SwitchOMatic runs at the data layer level. 
* Ability to retrieve past results. 
* Logging to a file. 
* Configure a run and run it multiple times. 
* Exposure of the service by WebAPI for other applications (client side implementations)


## Logging
Logging has been designed to be easily extensible. Logging providers that conform to the ILoggingRepository interface can be created and registered with the DIEngine; then when logging occurs all providers will be written to sequentially. Make sure that the logs 

## Testing
Some basic tests are included as well as the Postman collection to browse the API. The Post will fail if you include the 'save collection' arguement becuse there is no database configured (this will also cause an error and logging in the WebUI).

## Running
Just hit F5 in Visual Studio. you can change the location of the logs in the web.config folder under the 

## Next Steps and Enhancements
In order to move towards a production prototype the following suggestions should be considered:

* Connection to a database.
* The Viewmodel and Results model could be combined with inheritance and then hydrated with Automapper.
* Logging provider could be added for the database.
* Security could be added for the Webapp and the Webapi.
* Upgrade to Bootstrap 4.0