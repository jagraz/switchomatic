﻿using SwitchOMatic.Models;
using SwitchOMatic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SwitchOMatic.Controllers
{
    public class SwitchOMaticController : Controller
    {
        private readonly ISwitchOMaticService _switchOMaticService;
        private readonly ILoggingService _loggingService;

        public SwitchOMaticController(ISwitchOMaticService switchOMaticService, ILoggingService loggingService)
        {
            _switchOMaticService = switchOMaticService;
            _loggingService = loggingService;
        }

        // GET: SwitchOMatic
        public ActionResult Index()
        {
            return View(_switchOMaticService.GetResultsList());
        }

        // GET: SwitchOMatic/Details/5
        public ActionResult Details(int id)
        {
            var model = new SwitchOMaticViewModel(_switchOMaticService.GetResultsById(id));
            return View(model);
        }

        // GET: SwitchOMatic/Create
        public ActionResult Run()
        {
            return View("Details",new SwitchOMaticViewModel());
        }

        // POST: SwitchOMatic/Create
        [HttpPost]
        public ActionResult Run(SwitchOMaticSettings settings)
        {
            var timestart = DateTime.Now;

            try
            {                
               var results = _switchOMaticService.Run(settings);

                results.RunTime = DateTime.Now - timestart;

                return View("Details",new SwitchOMaticViewModel(results));
            }
            catch (Exception ex)
            {
                string errorMessage = string.Format("Uh oh. An error occurred: {0}", ex.Message);
                ViewBag.errorMessage = errorMessage;
                _loggingService.CreateLogEntry(DateTime.Now, "LoggingService", "An error occurred during Scenario save", errorMessage,"user@user.com"); 
                return View("Details",new SwitchOMaticViewModel());
            }
        }

       

       
    }
}
