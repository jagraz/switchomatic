﻿using SwitchOMatic.Models;
using SwitchOMatic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SwitchOMatic.Controllers
{
    public class SwitchOMaticServiceController : ApiController
    {
        public readonly ISwitchOMaticService _switchOMaticService;
        public SwitchOMaticServiceController(ISwitchOMaticService switchOMaticService)
        {
            _switchOMaticService = switchOMaticService;
        }
        // GET: api/SwitchOMaticService
        public IEnumerable<SwitchOMaticResults> Get()
        {
            var results = _switchOMaticService.GetResultsList();
            return results;
        }

        // GET: api/SwitchOMaticService/5
        public SwitchOMaticResults Get(int id)
        {
            var results = _switchOMaticService.GetResultsById(id);
            return results;
        }

        // POST: api/SwitchOMaticService
        public SwitchOMaticResults Post([FromBody]SwitchOMaticSettings settings)
        {
            var results = _switchOMaticService.Run(settings);
            return results;
        }

        // PUT: api/SwitchOMaticService/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/SwitchOMaticService/5
        public void Delete(int id)
        {
        }
    }
}
