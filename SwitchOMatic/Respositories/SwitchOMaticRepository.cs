﻿using SwitchOMatic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwitchOMatic.Respositories
{
    public class SwitchOMaticRepository : ISwitchOMaticRepository
    {
        private List<SwitchOMaticResults> GetResultsListInt() ///we use this to fake a database connection
        {
            var results = new List<SwitchOMaticResults>();

            results.Add(new SwitchOMaticResults { settings = new SwitchOMaticSettings { LightCount = 10, PeopleCount = 10, ScenarioName = "10 by 10" }, ResultsID = 1000, ScenarioName= "10 by 10", RunBy="Superman", RunOn = DateTime.Now.AddDays(-10) });
            results.Add(new SwitchOMaticResults { settings = new SwitchOMaticSettings { LightCount = 100, PeopleCount = 100, ScenarioName = "100 by 100" }, ResultsID = 1001, ScenarioName = "100 by 100", RunBy = "Aquaman", RunOn = DateTime.Now.AddDays(-6) });
            results.Add(new SwitchOMaticResults { settings = new SwitchOMaticSettings { LightCount = 50, PeopleCount = 1, ScenarioName = "50 by 10" }, ResultsID = 1002, ScenarioName = "50 by 10", RunBy = "Daffy Duck", RunOn = DateTime.Now.AddDays(-5) });
            results.Add(new SwitchOMaticResults { settings = new SwitchOMaticSettings { LightCount = 300, PeopleCount = 20, ScenarioName = "300 by 20" }, ResultsID = 1003 , ScenarioName = "300 by 20", RunBy = "The Joker", RunOn = DateTime.Now.AddDays(-3) });
            results.Add(new SwitchOMaticResults { settings = new SwitchOMaticSettings { LightCount = 1000, PeopleCount = 10000, ScenarioName = "Beatles in Town" }, ResultsID = 1004, ScenarioName = "Beatles in Town", RunBy = "IronMan", RunOn = DateTime.Now.AddDays(-1) });
   
            return results;
        }

        public List<SwitchOMaticResults> GetResultsList()
        {
            var results = GetResultsListInt();

            //populate the results from the repo method. we don't include the actual light results here to save resources; but in theory they would be coming out of the DB storage

            return results;
        }

        public SwitchOMaticResults GetResultsByID(int id)
        {
            var results = GetResultsListInt().Where(x => x.ResultsID == id).FirstOrDefault();

            //populate the results from the repo method, we only do this for details to improve performance

            results.results = SwitchLights(results.settings.LightCount, results.settings.PeopleCount); 

            return results;
        }

        public bool SaveResults(SwitchOMaticResults resultsToSave)
        {
            throw new Exception("Oops we couldn't save your stuff. The database crapped out"); 
        }

        //we are doing this to fake a database storage method
        public List<LightObject> SwitchLights(int lightCount, int peopleCount)
        {

            var LightList = InitLightObjectList(lightCount);

            for (int currentP = 1; currentP <= peopleCount;)
            {
                for (int currentL = (currentP - 1); currentL < lightCount;)
                {
                    LightList[currentL].Lit = !LightList[currentL].Lit;

                    LightList[currentL].TouchCount++;

                    currentL = currentL + currentP;
                }
                currentP++;
            }

            return LightList;
        }

        private List<LightObject> InitLightObjectList(int lightCount)
        {

            var results = new List<LightObject>();
            for (int i = 0; i < lightCount; i++)
            {
                results.Add(new LightObject());
            }

            return results;
        }
    }
}