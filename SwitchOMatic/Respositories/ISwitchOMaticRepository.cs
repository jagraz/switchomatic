﻿using System.Collections.Generic;
using SwitchOMatic.Models;

namespace SwitchOMatic.Respositories
{
    public interface ISwitchOMaticRepository
    {
        SwitchOMaticResults GetResultsByID(int id);
        List<SwitchOMaticResults> GetResultsList();
        bool SaveResults(SwitchOMaticResults resultsToSave);
        List<LightObject> SwitchLights(int lightCount, int peopleCount);
    }
}