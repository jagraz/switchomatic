﻿using System;

namespace SwitchOMatic.Respositories
{
    public interface ILoggingRepository
    {
        int priority { get; }

        void CreateLogEntry(DateTime loggedDate, string loggingSourceObjectID, string logActivity, string logDetails, string createdBy);
    }
}