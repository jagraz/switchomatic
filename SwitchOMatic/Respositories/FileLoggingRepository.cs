﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwitchOMatic.Models;

namespace SwitchOMatic.Respositories
{
    public class FileLoggingRepository : ILoggingRepository
    {
              

        public int priority { get { return 1; } }

        public void CreateLogEntry(DateTime loggedDate,  string loggingSourceObjectID, string logActivity, string logDetails, string createdBy)
        {
            var fileName = string.Format("{0}-{1}-{2}", DateTime.Now.Year, DateTime.Now.Month,DateTime.Now.Day)+ "-Log";
            string path = ConfigurationManager.AppSettings.Get("FileLoggingProviderPath") + fileName + ".txt";

            //create the file if we don't have it
            if (!File.Exists(path))
            {
              var filestream = File.Create(path, 128, FileOptions.Asynchronous);
                filestream.Close(); 
            }
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format("{0} - {1} - {2}", DateTime.Now, logActivity, logDetails));
                writer.Close();
            }


        }
    }
}
