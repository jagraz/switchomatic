﻿using SimpleInjector;
using SwitchOMatic.Respositories;
using SwitchOMatic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwitchOMatic.App_Start
{
    public class DIEngine
    {
        public static void RegisterTypes(Container container)
        {

            //register repositories
            container.Register<ISwitchOMaticRepository, SwitchOMaticRepository>();

            container.Collection.Register(typeof(ILoggingRepository), new[] {
                typeof(FileLoggingRepository)                  //add more here later
            });

            //register services

            container.Register<ISwitchOMaticService, SwitchOMaticService>();
            container.Register<ILoggingService, LoggingService>();

        }

        //you can use this for non-MVC methods like class libraries if you want to use the DI system out of the box.
        public static Container GetNewContainer()
        {
            var myContainer = new SimpleInjector.Container();
            RegisterTypes(myContainer);
            myContainer.Verify();
            return myContainer;
        }
    }
}
