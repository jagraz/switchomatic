﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SwitchOMatic.Models
{
    public class SwitchOMaticResults
    {
        [DisplayName("Results ID")]
        public int ResultsID { get; set; }
        public SwitchOMaticSettings settings { get; set; }
        public List<LightObject> results { get; set; }
        [DisplayName("Scenario Name")]
        public string ScenarioName { get; set; }
        [DisplayName("Who Ran it")]
        public string RunBy { get; set; }
        [DisplayName("When")]
        public DateTime RunOn { get; set; }

        [DisplayName("Run Time for Tests")]
        public TimeSpan RunTime {get;set;}

        public SwitchOMaticResults()
        {
            this.settings = new SwitchOMaticSettings();
            this.results = new List<LightObject>();
        }
    }
}