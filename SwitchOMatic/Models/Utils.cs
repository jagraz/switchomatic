﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace SwitchOMatic.Models
{
    public static class Utils
    {
        public static string JSONSerializeToString(Object objectData)
        {
            return JsonConvert.SerializeObject(objectData, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore 
            });
        }

        public static object JSONDeserializedFromString(string objectData, Type objectType)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Deserialize(objectData, objectType);
        }


        public static T JSONDeserializedFromString<T>(string objectData)
        {
            return (T)JSONDeserializedFromString(objectData, typeof(T));
        }

    }
}