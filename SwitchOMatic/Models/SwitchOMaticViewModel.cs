﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SwitchOMatic.Models
{
    public class SwitchOMaticViewModel
    {
        [DisplayName("Lights Now Lit")]
        public int Lightslit { get; set; }
        [DisplayName("Total Light Touches")]
        public int TotalLightTouches { get; set; }

        [DisplayName("Who Ran it")]
        public string RunBy { get; set; }
        [DisplayName("When")]
        public DateTime RunOn { get; set; }
        public int ResultsID { get; set; }

        public TimeSpan Runtime { get; set; }

        public List<LightObject> LightObjectList { get; set; }

        public SwitchOMaticSettings settings { get; set; }

        public SwitchOMaticViewModel()
        {
            this.settings = new SwitchOMaticSettings();
            this.LightObjectList = new List<LightObject>();
        }

        public SwitchOMaticViewModel(SwitchOMaticSettings settings, List<LightObject> results)
        {
            this.LightObjectList = results;
            this.settings = settings;
            this.Lightslit = this.LightObjectList.Where(x => x.Lit == true).Count();
            this.TotalLightTouches = this.LightObjectList.Sum(x=>x.TouchCount); 
        }

        public SwitchOMaticViewModel(SwitchOMaticResults results) : this(results.settings, results.results)
        {
            this.RunBy = results.RunBy;
            this.RunOn = results.RunOn;
            this.ResultsID = results.ResultsID;
            this.Runtime = results.RunTime;
        }
    }
}