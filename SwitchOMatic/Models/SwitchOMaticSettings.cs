﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SwitchOMatic.Models
{
    public class SwitchOMaticSettings
    {
        [Range(1,100000,ErrorMessage ="There must be at least 1 and less than 100001 people entering the room")]
        [Required(ErrorMessage ="There must be people entering the room")]
        [DisplayName("People Entering Room")]
        public int PeopleCount { get; set; }

        [Range(1, 100000, ErrorMessage = "There must be at least 1 and less than 100001 lights in the room")]
        [DisplayName("Lights In Room")]
        public int LightCount { get; set; }

        [DisplayName("Scenario Name")]
        public string ScenarioName { get; set; }

        [DisplayName("Save This Scenario")]
        public bool SaveScenario { get; set; }

        public SwitchOMaticSettings()
        {
            this.PeopleCount = 1;
        }
    }
}