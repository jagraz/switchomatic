﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SwitchOMatic.Models
{
    public class LightObject
    {
        [DisplayName("Light is Lit")]
        public bool Lit { get; set; }
        [DisplayName("Switch Touches")]
        public int TouchCount { get; set; }
    }
}
