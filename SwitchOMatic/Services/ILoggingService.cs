﻿using System;

namespace SwitchOMatic.Services
{
    public interface ILoggingService
    {
        void CreateLogEntry(DateTime loggedDate, string loggingSourceObjectID, string logActivity, string logDetails, string createdBy);
    }
}