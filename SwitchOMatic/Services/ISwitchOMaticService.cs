﻿using System.Collections.Generic;
using SwitchOMatic.Models;

namespace SwitchOMatic.Services
{
    public interface ISwitchOMaticService
    {
        SwitchOMaticResults GetResultsById(int id);
        List<SwitchOMaticResults> GetResultsList();
        SwitchOMaticResults Run(SwitchOMaticSettings settings);
        List<LightObject> SwitchLights(int lightCount, int peopleCount);
    }
}