﻿using SwitchOMatic.Respositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwitchOMatic.Services
{
    public class LoggingService : ILoggingService
    {
        public IEnumerable<ILoggingRepository> _loggingRepositories;

        public LoggingService(IEnumerable<ILoggingRepository> loggingRepositories)
        {
            _loggingRepositories = loggingRepositories;
        }

        public void CreateLogEntry(DateTime loggedDate, string loggingSourceObjectID, string logActivity, string logDetails, string createdBy)
        {
            foreach (var lr in _loggingRepositories) //we may have a bunch configured down the road
            {
                lr.CreateLogEntry(loggedDate, loggingSourceObjectID, logActivity, logDetails, createdBy);
            }
        }
    }
}