﻿using SwitchOMatic.Models;
using SwitchOMatic.Respositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwitchOMatic.Services
{
    public class SwitchOMaticService : ISwitchOMaticService
    {
        private readonly ISwitchOMaticRepository _switchOMaticRepository;

        public SwitchOMaticService(ISwitchOMaticRepository switchOMaticRepository)
        {
            _switchOMaticRepository = switchOMaticRepository;
        }

        public SwitchOMaticResults Run(SwitchOMaticSettings settings)
        {
            var results = new SwitchOMaticResults { settings = settings, results = this.SwitchLights(settings.LightCount, settings.PeopleCount) };

            if (settings.SaveScenario)
                _switchOMaticRepository.SaveResults(results);

            return results;

        }

        public List<LightObject> SwitchLights(int lightCount, int peopleCount)
        {

            var LightList = InitLightObjectList(lightCount);
 
            for (int currentP = 1; currentP <= peopleCount;)
            {
                for (int currentL = (currentP - 1); currentL < lightCount;)
                {
                    LightList[currentL].Lit = !LightList[currentL].Lit;

                    LightList[currentL].TouchCount++;

                    currentL = currentL + currentP;
                }
                currentP++;
            }

            return LightList;
        }

        private List<LightObject> InitLightObjectList(int lightCount)
        {

            var results = new List<LightObject>();
            for (int i = 0; i < lightCount; i++)
            {
                results.Add(new LightObject());
            }

            return results;
        }

        public List<SwitchOMaticResults> GetResultsList()
        {
            return _switchOMaticRepository.GetResultsList();
        }

        public SwitchOMaticResults GetResultsById(int id)
        {
            return _switchOMaticRepository.GetResultsByID(id);
        }

    }
}